create database travel;
use travel;
create table PASSENGER
 (Passenger_name varchar(20), 
  Category           varchar(20),
  Gender             varchar(20),
  Boarding_City      varchar(20),
  Destination_City   varchar(20),
  Distance           int,
  Bus_Type           varchar(20)
);
create table PRICE
(
	Bus_Type   varchar(20),
	Distance   int,
	Price      int
);
insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;

-- How many females and how many male passengers travelled for a minimum distance of 600 KM s?
select count(case when gender="M" then 1 end) AS Male,
		count(case when gender="F" then 1 end) AS Female,
		count(*) as Total from passenger where Distance>=600 group by Gender;

        
-- To Find the minimum ticket price for Sleeper Bus. 
select Bus_Type,min(Price) as Minimum_Price from price where Bus_Type="Sleeper";


-- Select passenger names whose names start with character 'S'
select Passenger_name from passenger where Passenger_name like 'S%';

 
-- Calculate price charged for each passenger displaying Passenger name, Boarding City, Destination City, Bus_Type, Price in the output.
select p.Passenger_name,p.Boarding_city,p.Destination_city,p.Bus_Type,q.Price from passenger p, price q where p.Distance=q.Distance AND p.Bus_type=q.Bus_type;
 

 -- Passenger name and  ticket price who travelled in Sitting bus for a distance of 1000 KMs.
select p.Passenger_name,p.Boarding_city,p.Destination_city,p.Bus_Type,q.Price from passenger p, price q where p.Distance=1000 and p.Bus_type="sitting";
 

 -- What will be the Sitting and Sleeper bus charge for Pallavi to travel from Bangalore to Panaji?
select distinct p.Passenger_name, p.Boarding_city as Destination_city,p.Destination_city as Boarding_city, p.Bus_Type, q.Price from passenger p1, price p2 where Passenger_name="Pallavi" and p.Distance=q.Distance;
 

 -- List the distances from the "Passenger" table which are unique (non-repeated distances) in descending order
select distinct Distance from passenger order by Distance desc;
 

 -- Display the passenger name and percentage of distance travelled by that passenger from the total distance travelled by all passengers without using user variables
select Passenger_name,Distance,Distance*100/(select sum(Distance) from passenger) as Percentage_Distance from passenger;
 

 -- Create a view to see all passengers who travelled in AC Bus
create view  PassengerView as select Passenger_name,Category from passenger where Category="AC";
select * from PassengerView;


-- Create a stored procedure to find total passengers travelled using Sleeper buses
DELIMITER []
create procedure TotalPassengers()
begin
select * from passenger where Bus_Type ='Sleeper';
end []
call TotalPassengers;


-- 11.Display 5 records at one time
select * from passenger limit 5;